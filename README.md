# Neulesuunnittelija prototyyppi
Tämä on pieni, nopea ja likainen prototyyppi kirjo neuleen suunnittelijalle.

Live demo suunnittelijaan löytyy osoitteesta:
http://themeininkistudios.com/neulontaproto/neulontaproto.html

Live demo lukijaan löytyy osoitteesta:
http://themeininkistudios.com/neulontaproto/lukuproto.html

# Knitting pattern designer prototype
This is a small, quick and dirty prototype for knitting pattern designer. Currently only
available in Finnish.

Live demo for designer is available at:
http://themeininkistudios.com/neulontaproto/neulontaproto.html

Live demo for reader is available at:
http://themeininkistudios.com/neulontaproto/lukuproto.html